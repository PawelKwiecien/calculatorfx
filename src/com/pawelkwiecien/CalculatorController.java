package com.pawelkwiecien;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;

import java.util.Formatter;

public class CalculatorController {

    private String calculationSymbol;
    private Double result = 0.0d;
    private Double firstValue;
    private Double memorizedValue = null;
    private Formatter formatter = new Formatter();
    private StringBuilder stringBuilder = new StringBuilder();

    @FXML
    private TextField textField;

    public void acButtonPressed() {
        stringBuilder = new StringBuilder();
        textField.setText("0");
        result = 0.0d;
        firstValue = null;
    }

    public void buttonOnePressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("1");
        textField.setText(stringBuilder.toString());
    }

    public void buttonTwoPressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("2");
        textField.setText(stringBuilder.toString());
    }

    public void buttonThreePressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("3");
        textField.setText(stringBuilder.toString());
    }

    public void buttonFourPressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("4");
        textField.setText(stringBuilder.toString());
    }

    public void buttonFivePressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("5");
        textField.setText(stringBuilder.toString());
    }

    public void buttonSixPressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("6");
        textField.setText(stringBuilder.toString());
    }

    public void buttonSevenPressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("7");
        textField.setText(stringBuilder.toString());
    }

    public void buttonEightPressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("8");
        textField.setText(stringBuilder.toString());
    }

    public void buttonNinePressed() {
        if (stringBuilder.length() < 12) stringBuilder.append("9");
        textField.setText(stringBuilder.toString());
    }

    public void buttonZeroPressed() {
        if (!textField.getText().equals("0")) {
            if (stringBuilder.length() < 12) stringBuilder.append("0");
            textField.setText(stringBuilder.toString());
        }
    }

    public void buttonDotPressed() {
        if (stringBuilder.length() < 12) {
            if (!textField.getText().contains(".")) {
                if (textField.getText().equals("0") && stringBuilder.length() == 0) {
                    stringBuilder.append("0.");
                } else {
                    stringBuilder.append(".");
                }
                textField.setText(stringBuilder.toString());
            }
        }
    }

    public void memoryPlusButtonPressed() {
        memoryClearButtonPressed();
        memorizedValue = Double.parseDouble(textField.getText());
        stringBuilder = new StringBuilder();
    }

    public void memoryClearButtonPressed() {
        memorizedValue = null;
    }

    public void memoryRecallButtonPressed() {
        if (memorizedValue != null) {
            textField.setText(memorizedValue.toString());
            formatter = new Formatter();
            stringBuilder = new StringBuilder(formatter.format("%f", memorizedValue).toString());
        }
    }

    public void backspaceButtonPressed() {
        if (stringBuilder.length() > 1) {
            stringBuilder.deleteCharAt(stringBuilder.length() - 1);
            textField.setText(stringBuilder.toString());
        } else {
            textField.setText("0");
        }
    }

    public void negativeButtonPressed() {
        if (stringBuilder.length() > 0 && !stringBuilder.toString().equals("0")) {
            if (stringBuilder.charAt(0) == '-') {
                stringBuilder.deleteCharAt(0);
                textField.setText(stringBuilder.toString());
            } else {
                stringBuilder.insert(0, "-");
                textField.setText(stringBuilder.toString());
            }
        }
    }

    public void plusButtonPressed() {
        if (calculationSymbol == null) {
            calculationSymbol = "+";
        }
        calculate();
        calculationSymbol = "+";
    }

    public void minusButtonPressed() {
        if (calculationSymbol == null) {
            calculationSymbol = "-";
        }
        calculate();
        calculationSymbol = "-";
    }

    public void multiplyButtonPressed() {
        if (calculationSymbol == null) {
            calculationSymbol = "*";
        }
        calculate();
        calculationSymbol = "*";
    }

    public void divideButtonPressed() {
        if (calculationSymbol == null) {
            calculationSymbol = "/";
        }
        calculate();
        calculationSymbol = "/";
    }

    public void equalsButtonPressed() {
        if (calculationSymbol != null) {
            calculate();
            stringBuilder = new StringBuilder();
            firstValue = result;
            calculationSymbol = null;
        }
    }

    private void calculate() {
        if (stringBuilder.length() > 0) {
            switch (calculationSymbol) {
                case "+":
                    if (firstValue == null) {
                        setFirstValue();
                    } else {
                        firstValue = Double.parseDouble(textField.getText());
                        result += firstValue;
                        displayResult();
                    }
                    break;
                case "-":
                    if (firstValue == null) {
                        setFirstValue();
                    } else {
                        firstValue = Double.parseDouble(textField.getText());
                        result -= firstValue;
                        displayResult();
                    }
                    break;
                case "*":
                    if (firstValue == null) {
                        setFirstValue();
                    } else {
                        firstValue = Double.parseDouble(textField.getText());
                        result *= firstValue;
                        displayResult();
                    }
                    break;
                case "/":
                    if (firstValue == null) {
                        setFirstValue();
                    } else {
                        if (!textField.getText().equals("0")) {
                            firstValue = Double.parseDouble(textField.getText());
                            result /= firstValue;
                            displayResult();
                        } else {
                            textField.setText("ERROR div/0");
                        }
                    }
                    break;
            }
        }
    }

    private void setFirstValue() {
        firstValue = Double.parseDouble(textField.getText());
        result = firstValue;
        stringBuilder = new StringBuilder();
    }

    private void displayResult() {
        StringBuilder tempBuilder = new StringBuilder();
        formatter = new Formatter();
        tempBuilder.append(formatter.format("%f", result));
        for (int i = tempBuilder.length() - 1; i > 0; i--) {
            if (tempBuilder.charAt(i) == '0') {
                tempBuilder.deleteCharAt(i);
            } else if (tempBuilder.charAt(i) == '.') {
                tempBuilder.deleteCharAt(i);
                break;
            } else {
                break;
            }
        }
        if (tempBuilder.length() > 12) {
            if (tempBuilder.toString().contains(".")) {
                while (tempBuilder.length() > 12) {
                    tempBuilder.deleteCharAt(tempBuilder.length() - 1);
                }
            } else {
                tempBuilder = new StringBuilder("ERROR num/length");
            }
        }
        stringBuilder = new StringBuilder();
        textField.setText(tempBuilder.toString());
    }

}
